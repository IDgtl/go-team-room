package events

import (
  "go-team-room/conf"
)

const (
  EVENT_USER_CONNECTED = iota
  EVENT_USER_DISCONNECTED
)

type (
  BaseEvent struct {
    Type string
  }

  ControlEvent struct {
    EvType  uint8
    Info    interface{}
  }
)

var (
  EventsFlow       = make(chan *ControlEvent, 200)
  OnlineUsers      = make(map[int64]int8)
  log = conf.GetLog()
)

func handleUserConnected(onlineUsers map[int64] int8, ev *ControlEvent) {

  userId, ok := ev.Info.(int64)
  if !ok {
    log.Warn("VERY BAD: Type assertion failed: ev info is not dto.ShortUser")
    return
  }

  onlineUsers[userId]++
  if onlineUsers[userId] == 1 {
    log.Printf("User with id %d connected.", userId)
  }
}

func handleUserDisconnected(onlineUsers map[int64] int8, ev *ControlEvent) {
  userId, ok := ev.Info.(int64)
  if !ok {
    log.Warn("VERY BAD: Type assertion failed: ev info is not dto.ShortUser")
    return
  }

  onlineUsers[userId]--
  if onlineUsers[userId] == 0 {
    delete(onlineUsers, userId)
    log.Printf("User with id %d disconected", userId)
  }
}

func EventsDispatcher() {
  for ev := range EventsFlow {
    if ev.EvType == EVENT_USER_CONNECTED {
      handleUserConnected(OnlineUsers, ev)
    } else if (ev.EvType == EVENT_USER_DISCONNECTED) {
      handleUserDisconnected(OnlineUsers, ev)
    }
  }
}
