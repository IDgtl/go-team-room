import Vue from 'vue'
import App from './App.vue'
import router  from './routers'
import BootstrapVue from 'bootstrap-vue'
import AsyncComputed from 'vue-async-computed'
import {store} from '../store/wsstore.js'


Vue.use(BootstrapVue);
Vue.use(AsyncComputed);


new Vue({
  router,
  store,
  el: '#app',
  render: h => h(App)
})
