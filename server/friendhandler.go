package server

import (
  "go-team-room/controllers"
  "net/http"
  "github.com/gorilla/mux"
  "strconv"
  "encoding/json"
  "go-team-room/models/dao/entity"
  "github.com/pkg/errors"
  //"fmt"
  //"fmt"
)

func getFriends(service controllers.FriendServiceInterface) http.HandlerFunc {
  return func(w http.ResponseWriter, r *http.Request) {
    idStr := mux.Vars(r)["user_id"]
    id, err := strconv.Atoi(idStr)
    if err != nil {
      responseError(w, err, http.StatusBadRequest)
      return
    }

    friends, err := service.GetFriends(int64(id))
    if err != nil {
      responseError(w, err, http.StatusBadRequest)
    }

    respBody, err := json.Marshal(friends)
    _, err = w.Write(respBody)
    if err != nil {
      responseError(w, err, http.StatusBadRequest)
      return
    }
  }
}


// Tanya's magic ----------------------------------------------------------------//

func unique(intSlice []int64) []int64 {
  keys := make(map[int64]bool)
  list := []int64{}
  for _, entry := range intSlice {
    if _, value := keys[entry]; !value {
      keys[entry] = true
      list = append(list, entry)
    }
  }
  return list
}

func getPeople(service controllers.FriendServiceInterface) http.HandlerFunc {
  return func(w http.ResponseWriter, r *http.Request) {
    idStr := mux.Vars(r)["user_id"]
    id, err := strconv.Atoi(idStr)
    if err != nil {
      responseError(w, err, http.StatusBadRequest)
      return
    }

    friends, err := service.GetFriends(int64(id))
    if err != nil {
      responseError(w, err, http.StatusBadRequest)
    }
    log.Println(friends)
    //log.Println(friends[0].ID)
	//
	   array := make([]int64, 0)


    for _, ok := range friends {
      log.Println(ok)
      log.Println(ok.ID)
      friendFr, err := service.GetFriends(int64(ok.ID))
      if err != nil {
        responseError(w, err, http.StatusBadRequest)
      }
      log.Println(friendFr)
      for _, ok := range friendFr {
         array = append(array, ok.ID)
        log.Println(array)
      }

    }

    array = unique(array)

    // delete my_id from array
    for i, val := range array {
      if (int64(val) == int64(id)) {
        array = append(array[:i], array[i+1:]...)
      }
    }

    //respBody, err := json.Marshal(friends)
    //_, err = w.Write(respBody)
    //if err != nil {
    //  responseError(w, err, http.StatusBadRequest)
    //  return
    //}
  }
}



func getUsersWithRequests(service controllers.FriendServiceInterface) http.HandlerFunc {
  return func(w http.ResponseWriter, r *http.Request) {
    idStr := mux.Vars(r)["user_id"]
    id, err := strconv.Atoi(idStr)
    if err != nil {
      responseError(w, err, http.StatusBadRequest)
      return
    }

    friends, err := service.GetUsersWithRequests(int64(id))
    if err != nil {
      responseError(w, err, http.StatusBadRequest)
    }

    respBody, err := json.Marshal(friends)
    _, err = w.Write(respBody)
    if err != nil {
      responseError(w, err, http.StatusBadRequest)
      return
    }
  }
}

func newFriendRequest(service controllers.FriendServiceInterface) http.HandlerFunc {
  return func(w http.ResponseWriter, r *http.Request) {
    var connection entity.Connection
    err := dtoFromReq(r, &connection)
    if err != nil {
      responseError(w, err, http.StatusBadRequest)
      return
    }

    err = service.NewFriendRequest(&connection)
    if err != nil {
      responseError(w, err, http.StatusBadRequest)
      return
    }

    w.WriteHeader(http.StatusOK)
  }
}

func replyToFriendRequest(service controllers.FriendServiceInterface) http.HandlerFunc {
  return func(w http.ResponseWriter, r *http.Request) {
    var connection entity.Connection
    err := dtoFromReq(r, &connection)
    if err != nil {
      responseError(w, err, http.StatusBadRequest)
      return
    }

    if connection.Status == entity.Approved {
      err = service.ApproveFriendRequest(&connection)
    } else if connection.Status == entity.Rejected {
      err = service.RejectFriendRequest(&connection)
    } else {
      responseError(w, errors.New("Invalid request reply"), http.StatusBadRequest)
    }
    if err != nil {
      responseError(w, err, http.StatusBadRequest)
      return
    }

    w.WriteHeader(http.StatusOK)
  }
}

func deleteFriendship(service controllers.FriendServiceInterface) http.HandlerFunc {
  return func(w http.ResponseWriter, r *http.Request) {
    var connection entity.Connection
    err := dtoFromReq(r, &connection)
    if err != nil {
      responseError(w, err, http.StatusBadRequest)
      return
    }

    err = service.DeleteFriendship(&connection)
    if err != nil {
      responseError(w, err, http.StatusBadRequest)
      return
    }

    w.WriteHeader(http.StatusOK)
  }
}
